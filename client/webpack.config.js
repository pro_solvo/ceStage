const path = require('path');
const webpack = require('webpack');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

var config = {
    context: __dirname + '/src',
    entry: {
      app: ['./index.jsx', './css/style.scss'],
    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].bundle.js'
    },
    watch: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                include: [path.resolve(__dirname, "src")],
                test: /\.jsx?$/,
                exclude: /node_modules|\.git/,
                // loader: "babel-loader",
                loader: 'babel-loader?-babelrc,+cacheDirectory,presets[]=es2015,presets[]=stage-0,presets[]=react',

                // query: {
                //     plugins: ['transform-runtime'],
                //     presets: ['es2015', 'stage-0', 'react'],
                // }
            },
              // scss
             {
                test: /\.(sass|scss)$/,
                exclude: /node_modules/,
                use: ExtractTextWebpackPlugin.extract({ fallback: 'style-loader', use: ['css-loader', 'sass-loader'] })
            }
        ]
    },
    plugins: [
      new ExtractTextWebpackPlugin("style.css"),
    ]
};

module.exports = config;

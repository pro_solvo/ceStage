from account.serializers import UserSerializer
from rest_framework import mixins
from rest_framework import generics
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework import renderers
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import viewsets
from django.contrib.auth.models import User
from account.permissions import IsOwnerOrReadOnly
from rest_framework.decorators import detail_route
# Create your views here.


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
        This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
